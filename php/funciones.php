<?php

	function conexion(){		
		$conexion = new mysqli("127.0.0.1","root","root","telelepiza");
		if(mysqli_connect_errno()){
			die("Error: " .mysqli_connect_errno());
		}
		return $conexion;
	}


	/* LOGIN.PHP */
	/**
	 * [login description]
	 * @param  String $usuario
	 * @param  String $password
	 * @return null
	 */
	function login($usuario,$password){
		$conexion = conexion();
		$sql = "select * from usuarios where nick='".$usuario."' and password='".$password."'";
		
		if($resultado=$conexion->query($sql)){
			if( $conexion->affected_rows == 1){
				if($row=$resultado->fetch_array(MYSQLI_ASSOC)){	
					$codigo = $row['codigo'];		
					$nombre =$row['nombre'];
					$direccion=$row['direccion'];
				}

				session_start();
				$_SESSION['id_usuario'] = $codigo;
				$_SESSION['nombre'] = $nombre;
				$_SESSION['direccion'] = $direccion;

				if($usuario=='MasterChief'){
					header('Location: cocina/cocina.php');
				}else{
					header('Location: interna/productos.php');
				}
				
			}else{
				header('Location: index.html');
			}
		}else{
			header('Location: index.html');
		}
		$conexion->close();
	}

	/* REGISTRO.PHP */
	function registro($nick,$nombre,$direccion,$password){
		$conexion = conexion();

		$sql = "select * from usuarios where nick='".$nick."';";
		if($resultado=$conexion->query($sql)){
			if($conexion->affected_rows == 1){
				return false;
			}else{
				$sql ="insert into usuarios (nick,nombre,direccion,password) values('".$nick."','".$nombre."','".$direccion."','".$password."') ";
				if($conexion->query($sql)){
					return true;
				}else{
					return false;
				}
			}
		}else{
			//Error al ejecutar Query
		}
		$conexion->close();
	}

	/* PRODUCTOS.PHP */
	function desplegable(){
		for($i=0;$i<=10;$i++){
			echo "<option value=$i>$i</option>";							
		}
	}

	function listarProductos(){
		$conexion = conexion();

		$sql = " select * from productos";
		if($resultado = $conexion->query($sql)){
			while ( $row=$resultado->fetch_array(MYSQLI_ASSOC) ) {
				echo "<article class=".$row['clase'].">";
				echo "<h3><a href=solo.php?c=".$row['codigo'].">".$row['nombre']."</a></h3>";
				$mini = substr($row['descripcion'], 0, 45);
				echo "<p>". $mini." ...</p>";
				echo "<h2 class='precio'>".$row['precio']." &#8364;</h2>";
				echo "<select name=cantidad[".$row['codigo']."]>";
				desplegable();  
				echo "</select>";
				echo "</article>";
			}
		}else{
			// Error al ejecutar Query
		}
		$conexion->close();
	}

	function listarSolo($codigo){
		$conexion = conexion();

		$sql = 'select * from productos where codigo='.$codigo.';';
		if($resultado=$conexion->query($sql)){
			if($row=$resultado->fetch_array(MYSQLI_ASSOC)){
				echo "<h1>".$row['nombre']."</h1>";
				echo "<p>".$row['descripcion']."</p>";
				echo "<p>Precio: ".$row['precio']."'00&#8364;</p>";
				echo "<p><img src='../comida/".$row['clase'].".jpg'></p>";
			}
		}else{
			// Error al ejecutar a Query
		}
		$conexion->close();
	}

	function vistaPedido($productos){
		$conexion = conexion();
		$total = 0;
		
		foreach ($productos as $codigo => $cantidad) {
			if($cantidad>0){
				$sql = "Select * from productos where codigo='".$codigo."'";
				if($resultado=$conexion->query($sql)){
					if($row=$resultado->fetch_array(MYSQLI_ASSOC)){
						echo "<h1>".$cantidad." x '".$row['nombre']."' unidad: ".number_format($row['precio'],2,',','')."&euro; total:".number_format($row['precio']*$cantidad,2,',',''). "&euro; </h1>";						
						$total = number_format($total+($row['precio']*$cantidad),2,',','');						
					}
				}else{
					//Error de la query
				}
			}
		}
		echo "<div class=total>";
		echo "<h3>Total Compra: ".$total."&euro;</h3>";
		echo "<h3>16% iva: ".number_format(($total+($total*0.10)),2,',','')."&euro;<h3>";	
		echo "</div>";
		$conexion->close();
	}

	function confirmarPedido($productos,$usuario){
		$conexion = conexion();
		$todoBien = true;
		$fecha=date("d-m-Y");
		if($conexion->query( "insert into facturas (fecha,usuario) values('".$fecha."','".$usuario."')" )){
			if($resultado = $conexion->query("select LAST_INSERT_ID()")){
				if($row=$resultado->fetch_array()){
					$numFactura=$row[0];										
				}
			}			
		}
		$conexion->close();	

		$todoBien = true;

		$conexion = conexion();		
		foreach ($productos as $codigo => $cantidad) {			
			if($cantidad>0){				
				if($conexion->query("insert into linea(codigo,factura,producto,cantidad) values(NULL,".$numFactura.",".$codigo.",".$cantidad.");")){
					// va bien
				}else{
					$todoBien = false;
				}				
			}
		}
		$conexion->close();

		if($todoBien){
			return true;
		}else{
			return false;
		}
		
	}

	function misPedidos($idUsuario){
		$conexion = conexion();

		$sql = "select * from facturas where usuario='".$idUsuario."' order by fecha desc;";

		if($resultado = $conexion->query($sql)){
			while($row=$resultado->fetch_array(MYSQLI_ASSOC)){
				echo "Factura: ".$row['codigo']." del dia: ".$row['fecha']."<br>";
			}
		}

		$conexion->close();
	}


	/* COCINA */

	function pedidosPendientes(){
		$conexion = conexion();

		if($resultado = $conexion->query(" select codigo from facturas where estado = 0")){
			while($row=$resultado->fetch_array()){
				echo '<div class=receta><h2>Factura numero: ' .$row[0].'</h2>';

				if($lineas = $conexion->query("select * from linea where factura =".$row[0].";")){
					while ($fila=$lineas->fetch_array(MYSQLI_ASSOC)) {

						if($nom = $conexion->query("select * from productos where codigo=".$fila['producto'].";")){
							if($nombres = $nom->fetch_array(MYSQLI_ASSOC)){
								echo '<li>'.$fila['cantidad'].' x '.$nombres['nombre'].' </li>';
							}
						}
						
					}
				}
				echo "<a href='cocinar.php?id=".$row[0]."'>Terminar</a></div>";
			}
		}


		$conexion->close();
	}


	function terminar($id){
		$conexion = conexion();
		$sql= "update facturas set estado = 1 where codigo =".$id."; ";
		if($conexion->query($sql)){
			return true;
		}else{
			return false;
		}
		$conexion->close();
	}



?>