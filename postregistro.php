<?php
	$codigo = $_GET['c'];
?>



<!DOCTYPE html>
<html>
<head>
	<title>Telelepiza HOME</title>
	<link rel="icon" href="img/favicon.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
	<link rel="stylesheet" type="text/css" href="css/estilobody.css">
</head>
<body>
	<nav>
		<div class="centrado">
				<a href="index.html"><img src="img/logo.png" id="logo" alt="Logotipo" /></a>
			<ul>
				<a href="index.html"><li>inicio</li></a>				
				<a href="registro.html"><li>Registro</li></a>
			</ul>
		</div>
	</nav>

	<section class="centrado">		
		<article class="cuadroInReg">
			<?php
				if ($codigo==1) {
					echo '<form method="POST" action="login.php">';
					echo '<h2>USUARIO REGISTRADO <span class="bien">CORRECTAMENTE</span></h2><br>';
					echo '<input type="text" name="usuario" placeholder="Tu Usuario" required>';
					echo '<input type="password" name="password" placeholder="Tu contrase&ntilde;a" required>';
					echo '<input type="submit" name="enviar" value="Iniciar Sesion">';
					echo '</form>';
				}else if($codigo==2){
					echo '<h2><span class="error">ERROR</span> NICK DE USUARIO YA EN USO</h2><br>';
					echo '<a href="registro.html">Registrarme ahora!</a>';
				}else if($codigo==3){
					echo '<h2><span class="error">ERROR</span> CONTRASE&Ntilde;AS NO COINCIDEN</h2><br>';
					echo '<a href="registro.html">Registrarme ahora!</a>';
				}
			?>			
		</article>		
	</section>

</body>
</html>