
<?php
	session_start();
	if(!isset($_SESSION['nombre'])){
		//Miro si existe una sesion, si no existe le mando a Index
		header('Location: ../index.html');
	}	
	require_once("../php/funciones.php");
?>

<!DOCTYPE html>
<html>
<head>
	<title>Telelepiza</title>
	<link rel="icon" href="../img/favicon.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="../css/estilo.css">
	<link rel="stylesheet" type="text/css" href="../css/estiloproductos.css">
	
</head>
<body>	

	<nav>
		<div class="centrado">
			<a href="productos.php"><img src="../img/logo.png" id="logo" alt="Logotipo" /></a>
			<ul>					
				<a href="productos.php"><li>Productos</li></a>
				<a href="mispedidos.php"><li>Mis Pedidos</li></a>
				<a href="salir.php"><li>Salir</li></a>	
			</ul>
				<div id="datosUsuario">
				<p><?php echo $_SESSION['nombre']; ?></p>
				<p><?php echo $_SESSION['direccion']; ?></p>				
			</div>
		</div>
	</nav>

	<header>
		<div class="centrado">
			<h1>Selecciona Nuestros Productos</h1>
		</div>
	</header>

	<section>
		<div class="centrado">	
			<form action="pedido.php" method="POST">
				<?php listarProductos();  ?>
				<article><input class="boton big" type="submit" name="enviar" value="Realizar Pedido"></article>
			<form>
		</div>	
	</section>	

</body>
</html>