
<?php
	session_start();
	if(!isset($_SESSION['nombre'])){		
		header('Location: ../index.html');
	}

	$productos = $_POST['cantidad'];

	foreach ($productos as $codigo => $cantidad) {
		if($cantidad>0){								
			$bien = true;		
		}							
	}

	if($bien==false){
		header('Location: productos.php');
	}

	//$productos = $_POST['cantidad'];	
	require_once("../php/funciones.php");
?>

<!DOCTYPE html>
<html>
<head>
	<title>Telelepiza</title>
	<link rel="icon" href="../img/favicon.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="../css/estilo.css">
	<link rel="stylesheet" type="text/css" href="../css/estiloproductos.css">
	
</head>
<body>
	<nav>
		<div class="centrado">
			<a href="productos.php"><img src="../img/logo.png" id="logo" alt="Logotipo" /></a>
			<ul>					
				<a href="productos.php"><li>Productos</li></a>
				<a href="mispedidos.php"><li>Mis Pedidos</li></a>
				<a href="salir.php"><li>Salir</li></a>	
			</ul>
			<div id="datosUsuario">
				<p><?php echo $_SESSION['nombre']; ?></p>
				<p><?php echo $_SESSION['direccion']; ?></p>				
			</div>

		</div>
	</nav>
	<header>
		<div class="centrado">
			<h1>CONFIRMAR PEDIDO</h1>
		</div>
	</header>

	<section>
		<div class="centrado">	
			<?php 
				vistaPedido($productos); 
			?>			
			<form action="confirmar.php" method="POST">				
				<input type="hidden" name="productos" value=<?php echo serialize($productos); ?> > 
				<input class="boton" type="submit" value="Confirmar Pedido">
			</form>
		</div>	
	</section>	

</body>
</html>