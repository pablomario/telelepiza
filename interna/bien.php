
<?php
	session_start();
	if(!isset($_SESSION['nombre'])){
		//Miro si existe una sesion, si no existe le mando a Index
		header('Location: ../index.html');
	}	
	require_once("../php/funciones.php");
?>

<!DOCTYPE html>
<html>
<head>
	<title>Telelepiza</title>
	<link rel="icon" href="../img/favicon.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="../css/estilo.css">
	<link rel="stylesheet" type="text/css" href="../css/estiloproductos.css">
	
</head>
<body>	

	<nav>
		<div class="centrado">
			<a href="productos.php"><img src="../img/logo.png" id="logo" alt="Logotipo" /></a>
			<ul>					
				<a href="productos.php"><li>Productos</li></a>
				<a href="mispedidos.php"><li>Mis Pedidos</li></a>
				<a href="salir.php"><li>Salir</li></a>	
			</ul>
				<div id="datosUsuario">
				<p><?php echo $_SESSION['nombre']; ?></p>
				<p><?php echo $_SESSION['direccion']; ?></p>				
			</div>
		</div>
	</nav>

	<header>
		<div class="centrado">
			<h1>Confirmacion</h1>
		</div>
	</header>

	<section>
		<div class="centrado">	
		<center>
			<h1>Su pedido esta en camino :) </h1>
			<img src="../img/enCamino.gif">
			<h2>Sigue disfrutando de nuestros <a href="productos.php"> productos </a> </h2>
		</center>
		</div>	
	</section>	

</body>
</html>