-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.50-community


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema telelepiza
--

CREATE DATABASE IF NOT EXISTS telelepiza;
USE telelepiza;

--
-- Definition of table `facturas`
--

DROP TABLE IF EXISTS `facturas`;
CREATE TABLE `facturas` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` varchar(100) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `estado` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facturas`
--

/*!40000 ALTER TABLE `facturas` DISABLE KEYS */;
INSERT INTO `facturas` (`codigo`,`fecha`,`usuario`,`estado`) VALUES 
 (24,'07-10-2014','2',0),
 (25,'07-10-2014','2',0),
 (26,'07-10-2014','2',1),
 (27,'07-10-2014','2',1),
 (28,'07-10-2014','2',1),
 (29,'07-10-2014','2',1),
 (30,'08-10-2014','2',1),
 (31,'08-10-2014','2',1),
 (32,'09-10-2014','',1);
/*!40000 ALTER TABLE `facturas` ENABLE KEYS */;


--
-- Definition of table `linea`
--

DROP TABLE IF EXISTS `linea`;
CREATE TABLE `linea` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `factura` int(11) NOT NULL,
  `producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `linea`
--

/*!40000 ALTER TABLE `linea` DISABLE KEYS */;
INSERT INTO `linea` (`codigo`,`factura`,`producto`,`cantidad`) VALUES 
 (71,24,2,1),
 (72,24,3,2),
 (73,25,1,2),
 (74,25,2,3),
 (75,25,3,3),
 (76,25,4,3),
 (77,25,5,2),
 (78,25,7,2),
 (79,25,8,3),
 (80,25,9,3),
 (81,25,10,8),
 (82,25,11,3),
 (83,26,2,1),
 (84,26,3,2),
 (85,28,2,2),
 (86,28,3,2),
 (87,29,3,3),
 (88,29,6,2),
 (89,29,10,3),
 (90,30,3,3),
 (91,31,2,1);
/*!40000 ALTER TABLE `linea` ENABLE KEYS */;


--
-- Definition of table `productos`
--

DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `precio` int(11) NOT NULL,
  `clase` varchar(50) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productos`
--

/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` (`codigo`,`nombre`,`descripcion`,`precio`,`clase`) VALUES 
 (1,'Hamburguesa Carne','hamburguesa de carne  con mas cosas como pepino patatas carne de rata lechuga cebolla y muchos ingredientes',5,'a'),
 (2,'Hamburguesa Patatas','Para extender nuestra linea de negocio creamos una Hamburguesa con maaaaa papaaaaas illo essale maaa papaaaaaas',4,'b'),
 (3,'Hamburguesa Lechuga','Mas verde y menos carne no se que idiota se pedira esto',4,'c'),
 (4,'Menu Carne','Para pesonras de verdad que les gustan tanto los animales que se los comen tiene algo de lechuga queso pero por dale cromatismo',7,'d'),
 (5,'Menu Escalope','Como el que hace tu madre pero metido en un bocata sin sala y con un poco de verde para que pase poco a poco la bebida sera vino calentorro y las patatas fritas estaran fria,lo que se conoce como el Yin y el Yan',12,'e'),
 (6,'Menu Come Piedras','Especil vegetarianos con mas lechuga y menos carne, el queso sale de las vacas asi que no se si podran comerlo y si no les gusta !Que coman piedras¡',50,'f'),
 (7,'Pizza Mercadona','Con la misma masa que un proton pero el doble de ingredientes una pizza gradisima y barata para los jueves noche y botellon',3,'g'),
 (8,'Pizza Panuzzi','En honor a la Pizzeria de Futurama donde Fry trabajo muchos años y el perro Simur vivia, por eso esta pizza tiene racion doble de pelos de perro en su masa',8,'h'),
 (9,'Pizza aNormal','para los amantes de la manzanita con mucha masa para los iDiots que con suerte se atragantan iMueren',8,'i'),
 (10,'Pizza Lo Mejor','Con los restos que dejaste de la comida del fin de semana en casa de tu madre y ella te decia comete eso que te dejas lo mejor, pues de eso es esta pizza',7,'j'),
 (11,'Ensalada C#','Productos frescos recien robados que incluye fauna salvaje como ranas babosas y demas elementos tipicos de una buena huerta.',6,'k');
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;


--
-- Definition of table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nick` varchar(45) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `direccion` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuarios`
--

/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`codigo`,`nick`,`nombre`,`direccion`,`password`) VALUES 
 (1,'peibol','Pablo Mario','c/ Pirata 9','password'),
 (2,'admin','Chema','------','admin'),
 (3,'mario','Mario','C/ Santa Ana 9','1234'),
 (4,'pablete','Pablete','Pablete','12345'),
 (5,'mayra','Mayra','c/ LALALALALALALA 14','madrid'),
 (6,'otro','Perico Palotes','c/ Ronda de Pinchos 10','12345'),
 (7,'MasterChief','Master Chief','c/ Halo Combat Evolved','123456');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
